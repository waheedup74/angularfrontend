import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LeftPanelComponent } from './left-panel/left-panel.component';
import { RightPanelComponent } from './right-panel/right-panel.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { ShortenPipe } from './shared/shorten.pipe';


@NgModule({
  declarations: [
    LeftPanelComponent,
    RightPanelComponent,
    PageNotFoundComponent,
    ShortenPipe
  ],

  imports: [
    CommonModule,
    RouterModule
  ],

  exports: [
    LeftPanelComponent,
    RightPanelComponent,
    PageNotFoundComponent,
    ShortenPipe,
    CommonModule
  ]
})
export class SharedModuleModule { }
