import { Injectable, EventEmitter } from "@angular/core";
import { SessionStorageService } from "angular-web-storage";
import { Subject } from "rxjs";
import { Router } from '@angular/router';

@Injectable()
export class LoginStatusService {

    private activatedForm: string;
    private isLoggedIn: boolean = false;

    public nextRouteName: string = "";
    public loggedUserData = null;

    // Used in 'Header Component' to keep updates, regarding user login and form status
    userLoginStatus = new EventEmitter<boolean>();

    // Used in 'Register Component' to keep update, regarding user signIn form errors and values
    updateActivatedForm = new Subject<string>();

    constructor(
        private session: SessionStorageService,
        private router: Router) {
    }

    public setForm(formNo: number) {
        localStorage.setItem("activatedForm", formNo + '');
        this.activatedForm = formNo + '';
        console.log(this.activatedForm);
        this.updateActivatedForm.next(this.activatedForm);
    }

    // *** Related to User Logged In Status **********************
    public userLoggedIn() { // Set user logged in status to 'true'
        this.isLoggedIn = true;
        this.userLoginStatus.emit(this.isLoggedIn);
    }

    signOut() {
        this.session.remove("authUserId");
        this.session.remove("email");
        this.userLoggedOut();
        this.removeRouteData();
        this.router.navigate(['/']);
    }

    public userLoggedOut() { // Set user logged in status to 'false'
        this.isLoggedIn = false;
        this.userLoginStatus.emit(this.isLoggedIn);
    }

    public isUserLoggedIn() {  // Checks if is user logged in or not using session

        if (this.session.get('email') != null && this.session.get('email') != '') {
            this.userLoggedIn();
        }
        else {
            this.userLoggedOut();
        }

        return this.isLoggedIn;
    }

    public getuserLogedinStatus() {  // To get the value of loggedIn 
        return this.isLoggedIn;
    }

    //--- Store the Routes-Path in LocalStorage to load it, on refreshing the page ------------------
    public setNextRouteName(nextRoute: string) {
        this.nextRouteName = nextRoute;
        localStorage.setItem("routerUrl", this.nextRouteName);
    }

    public getNextRouteName() {
        return this.nextRouteName;
    }

    public removeRouteData() {
        this.nextRouteName = '/';
        localStorage.setItem("routerUrl", this.nextRouteName);
    }
    //------------------------------------------------------------------------------------------------
}