import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LoginStatusService } from '../services/loginstatus.service';
import { SessionStorageService } from 'angular-web-storage';
import { BackendConnector } from '../services/backendconnector.service';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {

  userId: number = 0;
  loginFormType: number = 0;

  emailFieldInputValue: string = "";
  passwordFieldInputValue: string = "";
  message: string = "";

  passwordField: HTMLInputElement;
  passwordValidationStatus: boolean = false;
  passwordMatchStatus: boolean = false;
  isUserLoggedIn: boolean; // Is user is in logged-in state or not
  invalidPassword: boolean = false;
  invalidEmail: boolean = false;

  tempFormValues;
  @ViewChild('signinSecondForm') signinSecondForm;
  @ViewChild('signinFirstForm') signinFirstForm;

  constructor(
    private connectorService: BackendConnector,
    private loginService: LoginStatusService,
    private router: Router,
    public session: SessionStorageService) {

    // on refresh after login, open the same page by getting saved url in localStorage
    if (localStorage.getItem('routerUrl') != null)
      this.router.navigate(["/" + localStorage.getItem('routerUrl')]);
  }

  ngOnInit(): void {
    this.userId = parseInt(this.session.get('authUserId'));
    this.loginFormType = 0;
    this.isUserLoggedIn = this.loginService.isUserLoggedIn();

    this.loginService.userLoginStatus.subscribe(
      (userLoginStatus: boolean) => {
        this.isUserLoggedIn = userLoginStatus;
      }
    );
  }

  onSignIn(form: NgForm) {
    // Get user entered data from HTML-FORM
    const EmailorUsername = form.value.emailUsername;
    const password = form.value.password;

    if (!form.valid) {
      this.loginFormType = 1;

      setTimeout(() => {
        this.signinSecondForm.setValue({
          "emailUsername": EmailorUsername,
          "password": password,
        });
        this.tempFormValues = { "emailUsername": EmailorUsername, "password": password };
      }, 500);

      this.loginService.setForm(this.loginFormType);
    }

    else { // --- if form is valid ----------------------
      this.connectorService.signInRequest({ 'emailORusername': EmailorUsername, 'password': password }).then(
        (signInStatus: any) =>  // get data receieved from backend
        {
          console.log(signInStatus);
          if (!signInStatus.status) { // if response has 'false' in it, then signIn failed
            this.loginService.setForm(this.loginFormType);
          }
          else {
            if (this.loginFormType == 1) this.loginService.setForm(1);

            this.isUserLoggedIn = true;
            this.loginService.userLoggedIn(); // Update LoggedIn status
            this.loginService.loggedUserData = signInStatus.data;
            this.session.set("email", EmailorUsername); // store user data in session service
            this.session.set("authUserId", signInStatus.data.user_id);
            this.connectorService.getFriendsData();
            this.router.navigate(['landingpage/home']);
          }
        });
    }
  }

  updateFormValues(form: NgForm) {
    this.tempFormValues = { "emailUsername": form.value.emailUsername, "password": form.value.password };
  }

  // Show main-page (i.e. Register Component)
  LoadSignUp() {
    this.loginFormType = 0;
    setTimeout(() => { this.signinFirstForm.setValue(this.tempFormValues) }, 500);
    this.loginService.setForm(this.loginFormType);
  }

  LoadSignInForm() {
    this.loginService.setForm(this.loginFormType);
  }

  // Confirm Password Validation ---------------------------
  validateSignupConfirmPassword(element: HTMLInputElement) {
    if (this.passwordField != null) {
      if (element.value != "") {
        if (this.passwordField.value != element.value && this.passwordField.value != "")
          this.passwordMatchStatus = true;
        else
          this.passwordMatchStatus = false;
      }
      else if (element.value == "" && this.passwordField.value != "") {
        this.passwordMatchStatus = true;
      }
      else {
        this.passwordMatchStatus = false;
      }
    }
  }

  // **** SignIn Form Validation Functions ******************************************************
  validateSigninEmail(element: HTMLInputElement) {
    this.message = '';
    this.emailFieldInputValue = '';

    if (element.value.length >= 3)
      this.invalidEmail = false;
    else
      this.invalidEmail = true;
  }

  validateSigninPassword(element: HTMLInputElement) {
    this.message = '';
    this.passwordFieldInputValue = '';

    if (element.value.length >= 5)
      this.invalidPassword = false;
    else
      this.invalidPassword = true;
  }
  // *******************************************************************************************
}
