import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { Subscription, Subject } from 'rxjs';

import { BackendConnector } from '../services/backendconnector.service';
import { SharedDataService } from '../services/shareddata.service';
import { LoginStatusService } from '../services/loginstatus.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit, OnDestroy {

  loginSubscription: Subscription;
  intervalSubscription: Subscription;
  closeSubscription: Subscription;

  dates = [];
  months = [];
  years = [];

  //signinForm: FormGroup;
  signupForm: FormGroup;

  // Form input-fields validation variables
  nameValidationStatus: boolean = false;
  nameInputFieldError: boolean = false;

  emailValidationStatus: boolean = false;
  emailInputFieldError: boolean = false;

  genderValidationStatus: boolean = false;
  birthdayValidationStatus: boolean = false;
  passwordValidationStatus: boolean = false;
  passwordMatchStatus: boolean = false;

  activatedForm: number = 0;
  usernameErrorMsg: string = "";
  emailErrorMsg: string = "";
  message: string = "";
  passwordErrorMsg: string = "";
  friendSuggestionsLimit: number = 2;

  passwordField: HTMLInputElement;

  breakLineStatus: boolean = false;
  invalidPassword: boolean = false;
  invalidEmail: boolean = false;

  // variables used in 'Value- attribute' of input textfields
  emailFieldInputValue: string = "";
  passwordFieldInputValue: string = "";
  error: string = "";

  subject = new Subject<number>();

  constructor(private formBuilder: FormBuilder,
    private backendService: BackendConnector,
    private router: Router,
    public session: SessionStorageService,
    private loginService: LoginStatusService,
    private shareService: SharedDataService
  ) {

    this.shareService.generateYears();
    this.dates = this.shareService.dates;
    this.months = this.shareService.months;
    this.years = this.shareService.years;

    this.setSignUpDefaultValues();
  } //---- Constructor Ends --------------------------------------

  ngOnInit() { 
    // ... [THERE ARE TWO FORMS IN THIS COMPONENT "Registration form" and "SignIn form"] ...

    this.activatedForm = 0;
    console.log("reigster component: " + this.activatedForm);
    // update form error messages
    this.loginSubscription = this.loginService.updateActivatedForm.subscribe(
      (getActivatedForm: string) => {
        console.log(getActivatedForm);
        this.activatedForm = parseInt(getActivatedForm);
        this.message = "";
      });
  }

  
  onSignUp() {  
    // Gender Validation (If gender option is not selected by default)
    if (this.signupForm.value.gender == "gender") {
      if (this.signupForm.value.gender != null)
        this.genderValidationStatus = true;
      else
        this.genderValidationStatus = false;
    }

    // Birthday Validation
    if (this.signupForm.value.date == "" || this.signupForm.value.month == "" || this.signupForm.value.year == "")
      this.birthdayValidationStatus = true;
    else
      this.birthdayValidationStatus = false;

    // Storing User's Register Information in an object
    const signUpdata = {
      'username': this.signupForm.value.username,
      'email': this.signupForm.value.email,
      'gender': this.signupForm.value.gender,
      'date': this.signupForm.value.date,
      'month': this.signupForm.value.month,
      'year': this.signupForm.value.year,
      'password': this.signupForm.value.password,
    };

    this.backendService.signUpRequest(signUpdata).then(
      (responseStatus: any) => {
        if (!responseStatus.status) { // if response is false
          if (responseStatus.message == "nametaken") {
            this.nameValidationStatus = this.nameInputFieldError = true;
            this.usernameErrorMsg = "name already taken";
          }
          if (responseStatus.message == "emailtaken") {
            this.emailValidationStatus = this.emailInputFieldError = true;
            this.emailErrorMsg = "email already taken";
          }
        }
        else { // if response is true
          this.message = "signup successfull";
          this.setSignUpDefaultValues();
          this.router.navigate(['/']);
          this.removeMsg();
        }
      });
  }


  onLogin() {
    console.log("onLogin");
    var email_username;
    var password;

    // get and store input fields data
    if (this.emailFieldInputValue != '' && this.emailFieldInputValue != null)
      email_username = this.emailFieldInputValue;

    if (this.passwordFieldInputValue != '' && this.passwordFieldInputValue != null)
      password = this.passwordFieldInputValue;

    if (email_username != "" || password != "") { // if required fields are filled
      this.backendService.signInRequest({ 'emailORusername': email_username, 'password': password }).then(
        (signInStatusResponse: any) => {
          if (!signInStatusResponse.status) { // if response has 'false' in it, then signIn failed
            this.message = "incorrect email/password";
            this.emailFieldInputValue = '';
            this.passwordFieldInputValue = '';
          }
          else {
            this.loginService.userLoggedIn(); // update LoggedIn status
            this.session.set("email", email_username); // store user data in session service
            this.session.set("authUserId", signInStatusResponse.data.user_id);
            this.backendService.getFriendsData();
            this.loginService.setNextRouteName("landingpage/home");
            this.router.navigate(['landingpage/home']);
            this.emailFieldInputValue = '';
            this.passwordFieldInputValue = '';
          }
        }
      );
    }
  }

  // ****** SignUp Form Validation Functions **********************************************
  // Username Validation ---------------------------
  validateSignupName(element: HTMLInputElement) {
    this.nameInputFieldError = false;
    if (element.value.length > 0 && element.value.length < 3) {
      this.usernameErrorMsg = "name must be atleast 3 characters"
      this.nameValidationStatus = true;
    }
    else {
      this.usernameErrorMsg = "";
      this.nameValidationStatus = false;
    }
  }

  // Email Validation ------------------------------
  validateSignupEmail(element: HTMLInputElement) {
    this.emailInputFieldError = false;
    if (element.value != "" && this.signupForm.get('email').status == "INVALID") {
      this.emailErrorMsg = "invalid email address";
      this.emailValidationStatus = true;
    }
    else {
      this.emailErrorMsg = "";
      this.emailValidationStatus = false;
    }
  }

  // Birthday Validation ------------------------------
  validateSignupBirthdate(element: HTMLInputElement) {
    if (element.value != "" && (this.signupForm.value.month == "" || this.signupForm.value.year == "")) {
      this.birthdayValidationStatus = true;
    }
    else {
      this.birthdayValidationStatus = false;
    }
  }
  validateSignupBirthmonth(element: HTMLInputElement) {
    if (element.value != "" && (this.signupForm.value.date == "" || this.signupForm.value.year == "")) {
      this.birthdayValidationStatus = true;
    }
    else {
      this.birthdayValidationStatus = false;
    }
  }
  validateSignupBirthyear(element: HTMLInputElement) {
    if (element.value != "" && (this.signupForm.value.date == "" || this.signupForm.value.month == "")) {
      this.birthdayValidationStatus = true;
    }
    else {
      this.birthdayValidationStatus = false;
    }
  }

  // Password Validation ----------------------------
  validateSignupPassword(element: HTMLInputElement) {
    this.passwordField = element;
    this.message = '';

    if (element.value.length == 0) {
      this.breakLineStatus = false;
    }
    if ((element.value.length > 0 && element.value.length < 5) && this.signupForm.value.confirmpassword == "") {
      this.passwordMatchStatus = false;
    }
    if (element.value == this.signupForm.value.confirmpassword) {
      this.breakLineStatus = false;
      this.passwordMatchStatus = false;
      this.passwordValidationStatus = false;
    }

    if (element.value.length > 0 && element.value.length < 5) {
      this.breakLineStatus = true;
      this.passwordValidationStatus = true;
    }
    else if (element.value.length > 0 && element.value != this.signupForm.value.confirmpassword) {
      this.breakLineStatus = false;
      this.passwordMatchStatus = true;
      this.passwordValidationStatus = false;
    }
    else {
      this.passwordValidationStatus = false;
    }
  }

  // Confirm Password Validation ---------------------------
  validateSignupConfirmPassword(element: HTMLInputElement) {
    if (this.passwordField != null) {
      if (element.value != "") {
        if (this.passwordField.value != element.value && this.passwordField.value != "")
          this.passwordMatchStatus = true;
        else
          this.passwordMatchStatus = false;
      }
      else if (element.value == "" && this.passwordField.value != "") {
        this.passwordMatchStatus = true;
      }
      else {
        this.passwordMatchStatus = false;
      }
    }
  }

  // **** SignIn Form Validation Functions ******************************************************
  validateSigninEmail(element: HTMLInputElement) {
    this.message = '';
    this.emailFieldInputValue = '';

    if (element.value.length >= 3) {
      this.invalidEmail = false;
    }
    else {
      this.invalidEmail = true;
    }
  }

  validateSigninPassword(element: HTMLInputElement) {
    this.message = '';
    this.passwordFieldInputValue = '';

    if (element.value.length >= 5) {
      this.invalidPassword = false;
    }
    else {
      this.invalidPassword = true;
    }
  }
  // *******************************************************************************************

  setSignUpDefaultValues() {
    // initialize form values and validators
    this.signupForm = this.formBuilder.group({
      username: ['', [Validators.minLength(3), Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      gender: ['male', [Validators.required]],
      date: ['', [Validators.required]],
      month: ['', [Validators.required]],
      year: ['', [Validators.required]],
      password: ['', [Validators.minLength(5), Validators.required]],
      confirmpassword: ['', [Validators.required]]
    });
  }

  removeMsg() {
    setTimeout(() => { this.message = ""; }, 1500);
  }

  ngOnDestroy() {
    this.loginSubscription.unsubscribe();
    this.loginSubscription.unsubscribe();
  }

}// *** Class Ends ***
