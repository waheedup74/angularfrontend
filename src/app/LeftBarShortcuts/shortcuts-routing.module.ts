import { CommonModule } from '@angular/common';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FriendsectionComponent } from './friendsection/friendsection.component';
import { ShorcutsComponent } from './shorcuts.component';
import { MypageComponent } from './mypage/mypage.component';
import { PageHomeComponent } from './page-home/page-home.component';
import { SharedModuleModule } from '../shared-module.module';
import { CreatePageComponent } from './create-page/create-page.component';
import { RouteResolverService } from '../services/route-resolver.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '', component: ShorcutsComponent, children: [
      { path: 'friendsection', component: FriendsectionComponent },
      { path: 'mypage', component: MypageComponent },
      { path: 'page-home/:pageId/:page_name', component: PageHomeComponent, resolve: { pageResolver: RouteResolverService } },
      { path: 'create-page', component: CreatePageComponent }
    ]
  }
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    SharedModuleModule,
    RouterModule.forChild(routes)],

  declarations: [
    FriendsectionComponent,
    ShorcutsComponent,
    MypageComponent,
    PageHomeComponent
  ],

  exports: [RouterModule]
})
export class ShortcutsRoutingModule { }
