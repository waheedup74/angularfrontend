// ********** MODULES ****************************************
import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { NgxPopper } from 'angular-popper';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { StoreModule } from '@ngrx/store';

// ********** SERVICES **************************************
import { SocketService } from './services/socket.service';
import { AngularWebStorageModule } from 'angular-web-storage';
import { BackendConnector } from './services/backendconnector.service';
import { LoginStatusService } from './services/loginstatus.service';
import { AuthGuardService } from './services/authguard.service';
import { DeactivateGuardService } from './services/deactivateguard.service';
import { SharedDataService } from './services/shareddata.service';
import { RouteResolverService } from './services/route-resolver.service';
import { ScrollServiceService } from './services/scroll-service.service';

// ********** COMPONENTS *************************************
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import { ErrorpageComponent } from './errorpage/errorpage.component';

// ********** DIRECTIVES *************************************
import { HighlightDirective } from './directives/highlight.directive';
import { PropertyCheckDirective } from './directives/property-check.directive';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponentComponent,
    HighlightDirective,
    PropertyCheckDirective,
    ErrorpageComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AngularWebStorageModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPopper
  ],

  providers: [
    BackendConnector,
    LoginStatusService,
    AuthGuardService,
    DeactivateGuardService,
    SocketService,
    SharedDataService,
    RouteResolverService,
    ScrollServiceService,
    ScrollToService
  ],

  bootstrap: [AppComponent],

  entryComponents: [],

  exports: [
    ScrollServiceService,
    ScrollToService
  ]

})
export class AppModule { }
