import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AuthGuardService } from '../services/authguard.service';
import { DeactivateGuardService } from '../services/deactivateguard.service';

import { ReversePipe } from '../shared/reverse.pipe';

import { HomeComponent } from './home/home.component';
import { LandingpageComponent } from '../landingpage/landingpage.component';
import { TimelineComponent } from '../landingpage/timeline/timeline.component';
import { CreatePageComponent } from '../LeftBarShortcuts/create-page/create-page.component';
import { AllPostsComponent } from './all-posts/all-posts.component';
import { SharedModuleModule } from '../shared-module.module';
import { GroupsComponent } from './groups/groups.component';
import { GroupComponent } from './groups/group/group.component';
import { ErrorpageComponent } from '../errorpage/errorpage.component';


const timelineRoutes: Routes = [
  {
    path: '', canActivate: [AuthGuardService], component: LandingpageComponent, children: [
      { path: 'home', component: HomeComponent, canDeactivate: [DeactivateGuardService] },
      { path: 'timeline', component: TimelineComponent },
      {
        path: 'groups', component: GroupsComponent, children: [
          { path: ':pageNo', component: GroupComponent }
        ]
      }
    ]
  },
  { path: 'error', component: ErrorpageComponent, data: { messsage: 'Page not found!' } },
  { path: '**', redirectTo: '/landingpage/404' },
]

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    SharedModuleModule,
    RouterModule.forChild(timelineRoutes)
  ],
  declarations: [
    HomeComponent,
    LandingpageComponent,
    TimelineComponent,
    ReversePipe,
    CreatePageComponent,
    AllPostsComponent,
    GroupsComponent,
    GroupComponent
  ],
  exports: [RouterModule]
})
export class TimelineModule { }
