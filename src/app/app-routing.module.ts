import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RegisterComponent } from './register/register.component';


const appRoutes: Routes = [
  { path: '', component: RegisterComponent },
  { path: 'landingpage', loadChildren: './landingpage/timeline.module#TimelineModule' },
  { path: 'shortcuts', loadChildren: './LeftBarShortcuts/shortcuts-routing.module#ShortcutsRoutingModule' }
]

@NgModule({
  declarations: [],

  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, { useHash: true, preloadingStrategy: PreloadAllModules })
  ],

  exports: [RouterModule]
})

export class AppRoutingModule { }
