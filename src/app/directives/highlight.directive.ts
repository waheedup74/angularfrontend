import { Directive, Renderer2, OnInit, ElementRef, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit {

  @Input() defaultColor: string = 'white';
  @Input() highLightColor: string = '#e9e9f5';
  @HostBinding('style.backgroundColor') bgColor: string = 'white';

  constructor(private renderer: Renderer2, private eleRef: ElementRef) { }

  ngOnInit() {
    this.bgColor = this.defaultColor;
  }

  @HostListener('mouseenter') mouseenter(eventData: Event) {
    this.bgColor = this.highLightColor;
  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.bgColor = this.defaultColor;
  }
}
